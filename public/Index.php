<?php

header('Access-Control-Allow-Origin: http://localhost:3000');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin,Content-Type,Accept,Authorization,X-Features');
header('Access-Control-Allow-Credentials: true');


use Core\Application;
use Core\Components\Container\Container;
use DI\ContainerBuilder;

define('ROOT_PATH', __DIR__ . '/../');
define('CORE_PATH', __DIR__ . '/../src/Core/');

require ROOT_PATH . 'vendor/autoload.php';

$containerBuilder = new ContainerBuilder;
$containerBuilder->addDefinitions('../src/Application/Config/dependencies.php');
$container = $containerBuilder->build();
try {
	Container::init($container);

	Application::getInstance();

}catch(Exception $e){
	dump($e);
}
