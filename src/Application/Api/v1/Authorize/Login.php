<?php

namespace App\Api\v1\Authorize;


use Core\Components\Api\Api;
use Core\Components\Auth\OAuth\Auth;
use Core\Components\Http\Request;
use Core\Model\AdminSection\UserCommon;
use Core\Utils\ApiParser\ApiParser;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Логин
 */
class Login implements Api
{
	/**
	 * @return string
	 */
	public function execute(): string
	{
		$params = (new ApiParser())->rebuildResultFromApi((new Request())->getRequest()->getContent());
		(new Auth())->authorization();
		if ((new UserCommon())->getCheckUser($params)) {

			return JsonResponse::create([
				'status' => 'success',
				'data' => [
					'msg' => 'Пользователь есть в системе!',
					'login' => 'success',
					'redirectUrl' => 'http://localhost:3001/'
				]
			], 200, [
				'Content-Type: application/json', 'charset=utf-8'
			])->send();
		}
		return JsonResponse::create([
			'status' => 'success',
			'data' => [
				'msg' => 'Пользователя нету в системе!',
				'login' => 'error',
				'redirectUrl' => 'error'
			]
		], 200, [
			'Content-Type: application/json', 'charset=utf-8'
		])->send();
	}

}
